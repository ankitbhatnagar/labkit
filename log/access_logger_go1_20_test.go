//go:build go1.20

package log

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestAccessLoggerFlushable(t *testing.T) {
	rw := httptest.NewRecorder()
	lrw := &loggingResponseWriter{rw: rw}
	rc := http.NewResponseController(lrw)

	err := rc.Flush()
	require.NoError(t, err, "the underlying response writer is not flushable")
}
