package errortracking

import (
	"github.com/getsentry/sentry-go"
)

type InitializationOption = TrackerOption

// Initialize initializes global error tracking.
// Call this once on the program start.
func Initialize(opts ...InitializationOption) error {
	err := sentry.Init(trackerOptionsToSentryClientOptions(opts...))
	if err != nil {
		return err
	}
	defaultTracker = newSentryTracker(sentry.CurrentHub())
	return nil
}
