package errortracking

import (
	"github.com/getsentry/sentry-go"
)

// WithUserID allows to add a user id to the error.
func WithUserID(userID string) CaptureOption {
	return func(config *captureConfig, event *sentry.Event) {
		event.User.ID = userID
	}
}
