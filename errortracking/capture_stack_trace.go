package errortracking

import (
	"github.com/getsentry/sentry-go"
)

// WithStackTrace allows to extract the stack trace from an error
// and report it to Sentry
func WithStackTrace() CaptureOption {
	return func(config *captureConfig, event *sentry.Event) {
		config.attachStackTrace = true
	}
}
