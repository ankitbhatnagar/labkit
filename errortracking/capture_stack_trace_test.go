package errortracking

import (
	"testing"

	"github.com/getsentry/sentry-go"
	"github.com/stretchr/testify/assert"
)

func TestWithStackTrace(t *testing.T) {
	event := sentry.NewEvent()
	config := &captureConfig{}

	WithStackTrace()(config, event)

	assert.True(t, config.attachStackTrace)
}
