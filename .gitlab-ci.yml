default:
  image: golang:1.20
  tags:
    - gitlab-org

include:
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'

variables:
  REPO_NAME: gitlab.com/gitlab-org/labkit
  GO_SEMANTIC_RELEASE_VERSION: 2.12.0
  SAST_EXCLUDED_ANALYZERS: "eslint,gosec,nodejs-scan"

  # Note, GOLANGCI_LINT_VERSION should match .tool-versions!
  GOLANGCI_LINT_VERSION: 1.41.1

.go-version-matrix:
  parallel:
    matrix:
      - GO_VERSION: ["1.18", "1.19", "1.20"]

backwards_compat:
  image: registry.gitlab.com/gitlab-org/gitlab-build-images/debian-bullseye-golang-1.20-rust-1.65:git-2.36
  parallel:
    matrix:
      - REPO_AND_DIR:
        - https://gitlab.com/gitlab-org/gitaly.git
        - https://gitlab.com/gitlab-org/container-registry.git
        - https://gitlab.com/gitlab-org/gitlab-shell.git
        - https://gitlab.com/gitlab-org/gitlab-pages.git
        - https://gitlab.com/gitlab-org/gitlab.git workhorse
  stage: verify
  script:
    - ./backwards_compat.sh ${REPO_AND_DIR}

stages:
  - build
  - verify
  - release

build:
  image: golang:${GO_VERSION}-alpine
  extends: .go-version-matrix
  stage: build
  before_script:
    - apk add --no-cache git
  script:
    - ./compile.sh

test:
  extends: .go-version-matrix
  image: golang:${GO_VERSION}
  stage: verify
  script:
    - ./test.sh

test-fips:
  image: registry.gitlab.com/gitlab-org/gitlab-runner/go-fips:1.18
  stage: verify
  variables:
    REQUIRED_BUILD_TAGS: fips
    CGO_ENABLED: 1
  script:
    - ./test.sh

# The verify step should always use the same version of Go as devs are
# likely to be developing with to avoid issues with changes in these tools
# between go versions. Since these are simply linter warnings and not
# compiler issues, we only need a single version

sast:
  stage: verify

dependency_scanning:
  stage: verify

mod tidy:
  stage: verify
  script:
    - ./tidy.sh

shellcheck:
  image:
    name: koalaman/shellcheck-alpine:latest
  stage: verify
  rules:
    - changes:
      - "**/*.sh"
  script:
    - shellcheck -s sh -S style $(ls *.sh | grep -v downstream)

lint:
  image:
    name: golangci/golangci-lint:v${GOLANGCI_LINT_VERSION}-alpine
    entrypoint: ["/bin/sh", "-c"]
  # git must be installed for golangci-lint's --new-from-rev flag to work.
  before_script:
  - apk add --no-cache git jq
  stage: verify
  script:
    - ./lint.sh
  artifacts:
    reports:
      codequality: gl-code-quality-report.json
    paths:
      - gl-code-quality-report.json

commitlint:
  stage: verify
  image: node:14-alpine3.12
  before_script:
  - apk add --no-cache git
  - npm install
  script:
  - npx commitlint --from ${CI_MERGE_REQUEST_DIFF_BASE_SHA} --to HEAD --verbose
  rules:
  - if: $CI_MERGE_REQUEST_IID

release:
  image: node:14
  stage: release
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: manual
  script:
    - npm install
    - $(npm bin)/semantic-release
