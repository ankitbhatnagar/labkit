package tracing

import (
	"github.com/opentracing/opentracing-go"
	"gitlab.com/gitlab-org/labkit/tracing/impl"
)

// IsSampled returns the sampling status (true/false) of a span. This function
// wraps around the actual implementation in `impl` packet. Technically, we don't
// need this wrapper, but the `impl` package contains detailed implementation.
// Most consumers import `gitlab.com/gitlab-org/labkit/tracing`
func IsSampled(span opentracing.Span) bool {
	// IsSampled returns the sampling status (true/false) of a span. Most
	// implementations support checking this status. Unfortunately, we hide the
	// tracing implementations behind unified opentracing interfaces. The single
	// input is an opentracing.Span interface. As a result, we need to type-cast it
	// back to all supported implementations.
	// This status is particularly useful when a span is not free to collect, or
	// has to turn on some special configs. One example is Git Trace2. It's a
	// built-in observability tool that provides a deeper look into Git processes.
	// Enabling this feature is not expensive, but not cheap either. We don't want
	// to enable it for all Git processes. So, it makes sense to collect such data
	// when the parent span is sampled.
	return impl.IsSampled(span)
}
