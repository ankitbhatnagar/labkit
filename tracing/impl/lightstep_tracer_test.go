// +build tracer_static,tracer_static_lightstep

package impl

import (
	"fmt"
	"github.com/lightstep/lightstep-tracer-go"
	"github.com/opentracing/opentracing-go"
	"github.com/stretchr/testify/require"
	"testing"

	"gitlab.com/gitlab-org/labkit/tracing/connstr"
)

func Test_lightstepTracerFactory(t *testing.T) {
	tests := []struct {
		connectionString string
		wantErr          bool
		strict           bool
	}{
		{
			connectionString: "opentracing://lightstep",
			wantErr:          true,
			strict:           true,
		},
		{
			connectionString: "opentracing://lightstep?access_token=12345",
			wantErr:          false,
			strict:           true,
		},
		{
			connectionString: "opentracing://lightstep?access_token=12345&relaxed",
			wantErr:          false,
			strict:           false,
		},
		{
			connectionString: "opentracing://lightstep?access_token=12345&strict",
			wantErr:          true,
			strict:           true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.connectionString, func(t *testing.T) {
			_, options, err := connstr.Parse(tt.connectionString)
			if err != nil {
				t.Errorf("TracerFactory() error = unable to parse connection string: %v", err)
			}
			if tt.strict {
				options[keyStrictConnectionParsing] = "1"
			}

			options["service_name"] = "test"

			gotTracer, gotCloser, err := lightstepTracerFactory(options)

			if (err != nil) != tt.wantErr {
				t.Errorf("TracerFactory() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !tt.wantErr {
				if gotTracer == nil {
					t.Errorf("TracerFactory() expected a tracer, got nil")
				}
				if gotCloser == nil {
					t.Errorf("TracerFactory() expected a closed, got nil")
				}
			}
		})
	}
}

func TestIsSampled_lightstep(t *testing.T) {
	t.Parallel()

	for _, tc := range []struct {
		desc       string
		connection string
		sampled    bool
	}{
		{
			desc:       "lightstep sampled",
			connection: "opentracing://lightstep?access_token=12345&relaxed",
			sampled:    true,
		},
		{
			desc:       "lightstep not sampled",
			connection: "opentracing://lightstep?access_token=12345&relaxed",
			sampled:    false,
		},
	} {
		t.Run(tc.desc, func(t *testing.T) {
			_, options, err := connstr.Parse(tc.connection)
			require.NoError(t, err)
			options["service_name"] = "test"

			lightstepTracer, closer, err := lightstepTracerFactory(options)
			require.NoError(t, err)

			var opt opentracing.StartSpanOption
			if tc.sampled {
				opt = lightstep.SetSampled("true")
			} else {
				opt = lightstep.SetSampled("false")
			}

			span := lightstepTracer.StartSpan("rootSpan", lightstep.SetTraceID(1), opt)
			for i := 0; i < 10; i++ {
				require.Equal(t, tc.sampled, IsSampled(span))
				span = lightstepTracer.StartSpan(fmt.Sprintf("span%d", i), opentracing.ChildOf(span.Context()))
			}
			require.NoError(t, closer.Close())
		})
	}
}
