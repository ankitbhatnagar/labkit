// +build tracer_static,tracer_static_jaeger

package impl

import (
	"fmt"
	"github.com/opentracing/opentracing-go"
	"github.com/stretchr/testify/require"
	"testing"

	"gitlab.com/gitlab-org/labkit/tracing/connstr"
)

func TestTracerFactory(t *testing.T) {
	tests := []struct {
		connectionString string
		wantErr          bool
		strict           bool
	}{
		{
			connectionString: "opentracing://jaeger",
			wantErr:          false,
			strict:           true,
		},
		{
			connectionString: "opentracing://jaeger?debug=true",
			wantErr:          false,
			strict:           true,
		},
		{
			connectionString: "opentracing://jaeger?sampler=const&sampler_param=0",
			wantErr:          false,
			strict:           true,
		},
		{
			connectionString: "opentracing://jaeger?sampler=probabilistic&sampler_param=0.1",
			wantErr:          false,
			strict:           true,
		},
		{
			connectionString: "opentracing://jaeger?http_endpoint=http%3A%2F%2Flocalhost%3A14268%2Fapi%2Ftraces",
			wantErr:          false,
			strict:           true,
		},
		{
			connectionString: "opentracing://jaeger?udp_endpoint=10.0.0.1:1234",
			wantErr:          false,
			strict:           true,
		},
		{
			connectionString: "opentracing://jaeger?service_name=api",
			wantErr:          false,
			strict:           true,
		},
		{
			connectionString: "opentracing://jaeger?invalid_option=blah&relaxed",
			wantErr:          false,
			strict:           false,
		},
		{
			connectionString: "opentracing://jaeger?invalid_option=blah&strict",
			wantErr:          true,
			strict:           true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.connectionString, func(t *testing.T) {
			_, options, err := connstr.Parse(tt.connectionString)
			if err != nil {
				t.Errorf("TracerFactory() error = unable to parse connection string: %v", err)
			}
			if tt.strict {
				options[keyStrictConnectionParsing] = "1"
			}

			options["service_name"] = "test"

			gotTracer, gotCloser, err := jaegerTracerFactory(options)

			if (err != nil) != tt.wantErr {
				t.Errorf("TracerFactory() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !tt.wantErr {
				if gotTracer == nil {
					t.Errorf("TracerFactory() expected a tracer, got nil")
				}
				if gotCloser == nil {
					t.Errorf("TracerFactory() expected a closed, got nil")
				}
			}
		})
	}
}

func TestIsSampled_jaeger(t *testing.T) {
	t.Parallel()

	for _, tc := range []struct {
		desc       string
		connection string
		sampled    bool
	}{
		{
			desc:       "jaeger sampled",
			connection: "opentracing://jaeger?sampler=probabilistic&sampler_param=1",
			sampled:    true,
		},
		{
			desc:       "jaeger not sampled",
			connection: "opentracing://jaeger?sampler=probabilistic&sampler_param=0",
			sampled:    false,
		},
	} {
		t.Run(tc.desc, func(t *testing.T) {
			_, options, err := connstr.Parse(tc.connection)
			require.NoError(t, err)
			options["service_name"] = "test"

			jaegerTracer, closer, err := jaegerTracerFactory(options)
			require.NoError(t, err)

			span := jaegerTracer.StartSpan("rootSpan")
			for i := 0; i < 10; i++ {
				require.Equal(t, tc.sampled, IsSampled(span))
				span = jaegerTracer.StartSpan(fmt.Sprintf("span%d", i), opentracing.ChildOf(span.Context()))
			}
			require.NoError(t, closer.Close())
		})
	}
}
