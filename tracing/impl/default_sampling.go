package impl

import (
	"github.com/opentracing/opentracing-go"
)

var IsSampled = func(span opentracing.Span) bool {
	spanContext := span.Context()

	// Other non-exported context with conditional build tags. They must
	// implement samplingChecker interface.
	if spanSampler, ok := spanContext.(samplingChecker); ok {
		return spanSampler.IsSampled()
	}

	return false
}

// samplingChecker is an interface consisting of one function that returns
// the sampling status. Some implementations follow opentracing. Labkit doesn't
// need to convert. Some others, such as stackdriver, implement opentracing
// wrappers. Those wrappers are internal, and conditional built when receiving
// corresponding build tag. As a result, such wrappers are not available for
// casting in IsSampled function above.
// Such implementations have to take a detour by implementing this interface.
type samplingChecker interface {
	IsSampled() bool
}
