package http_round_tripper

type config struct {
	labelValues map[string]string
}

type factoryConfig struct {
	namespace              string
	subsystem              string
	requestDurationBuckets []float64
	labels                 []string
}

// FactoryOption is used to pass options in NewFactory.
type FactoryOption func(*factoryConfig)

func applyFactoryOptions(opts []FactoryOption) factoryConfig {
	config := factoryConfig{
		subsystem: "http",
		requestDurationBuckets: []float64{
			0.005, /* 5ms */
			0.025, /* 25ms */
			0.1,   /* 100ms */
			0.5,   /* 500ms */
			1.0,   /* 1s */
			10.0,  /* 10s */
			30.0,  /* 30s */
			60.0,  /* 1m */
			300.0, /* 5m */
		},
		labels: []string{"code", "method"},
	}
	for _, v := range opts {
		v(&config)
	}

	return config
}

// WithNamespace will configure the namespace to apply to the metrics.
func WithNamespace(namespace string) FactoryOption {
	return func(config *factoryConfig) {
		config.namespace = namespace
	}
}

// WithLabels will configure additional labels to apply to the metrics.
func WithLabels(labels ...string) FactoryOption {
	return func(config *factoryConfig) {
		config.labels = append(config.labels, labels...)
	}
}

// WithRequestDurationBuckets will configure the duration buckets used for
// incoming request histogram buckets.
func WithRequestDurationBuckets(buckets []float64) FactoryOption {
	return func(config *factoryConfig) {
		config.requestDurationBuckets = buckets
	}
}
