#!/usr/bin/env sh

set -eux

# Check go tidy
git diff go.sum go.mod > /tmp/gomod-"${CI_JOB_ID}"-before
git diff go.sum go.mod > /tmp/gomod-"${CI_JOB_ID}"-after
go mod tidy
diff -U0 /tmp/gomod-"${CI_JOB_ID}"-before /tmp/gomod-"${CI_JOB_ID}"-after

